# Project Rundown

## Index

1. Challenge
2. Point Breakdown
3. Initial Observations 
4. Firmware breakdown
5. Solution

---

## 1. Challenge

---

## 2. Point Breakdown

---

## 3. Initial Observations 

Running the `file` command on the dumped binary gave us the following output

```
flash_dump.bin: , code offset 0x201+3, OEM-ID "@��@", Bytes/sector 26688, sectors/cluster 5, FATs 16, root entries 16, sectors 20480 (volumes <=32 MB), Media descriptor 0xf5, sectors/FAT 16400, sectors/track 19228, FAT (12 bit by descriptor)
```

This does not appear to give us a whole lot of information but may be of use down the road. Lets try running binwalk on the binary with: `binwalk -v flash_dump.bin`

```
Scan Time:     2019-03-04 20:30:47
Target File:   /Users/daniel/Src/adam_project1/embedded_device/flash_dump.bin
MD5 Checksum:  9e73841b9f41f064a41980e9ccfd4ce7
Signatures:    391

DECIMAL       HEXADECIMAL     DESCRIPTION
--------------------------------------------------------------------------------
272617        0x428E9         Base64 standard index table
```

A bit more information, but also leaves us with more questions. !Why are there 391 signatures? !What is a Base64 standard index table. We can find out more about these later but it doesnt seem like there is much for us to glean directly from this.

There is a tool created for analysis on the ESP series called esptool and can be found [here](https://github.com/espressif/esptool).

Using `esptool.py image_info flash_dump.bin` we can get some information about the flash dump.

```
esptool.py v2.6
Image version: 1
Entry point: 4010f29c
1 segments
Segment 1: len 0x00568 load 0x4010f000 file_offs 0x00000008
Checksum: 2d (valid)
```

---

## 4. Firmware breakdown

From [this](https://github.com/espressif/esptool/wiki/Firmware-Image-Format) website we can see the breakdown of the flash dump binary file. This could come in handy if we need to write a unpacker/analyser/loader/etc.

| Byte | Description                                                                                                            |
|------|------------------------------------------------------------------------------------------------------------------------|
| 0    | Always 0xE9                                                                                                            |
| 1    | Number of segments                                                                                                     |
| 2    | SPI Flash Interface (0 = QIO, 1 = QOUT, 2 = DIO, 0x3 = DOUT)                                                           |
| 3    | High four bits: 0 = 512K, 1 = 256K, 2 = 1M, 3 = 2M, 4 = 4M, Low four bits: 0 = 40MHz, 1= 26MHz, 2 = 20MHz, 0xf = 80MHz |
| 4-7  | Entry point                                                                                                            |
| 8-n  | Segments                                                                                                               |

Additionally we see that each segment has the following format

| Byte  | Description   |
|-------|---------------|
| 0-3   | Memory offset |
| 4-7   | Segment size  |
| 8...n | Data          |

Using `hexdump -v -n 64 -C flash_dump.bin` we print out the first 64 bits for analysis.

```
00000000  e9 01 02 40 9c f2 10 40  00 f0 10 40 68 05 00 00  |...@...@...@h...|
00000010  10 10 00 00 50 f5 10 40  1c 4b 00 40 cc 24 00 40  |....P..@.K.@.$.@|
00000020  ff ff ff 3f ff ff 0f 40  ff 7f 10 40 ff ff ff 5f  |...?...@...@..._|
00000030  f0 ff ff 3f 00 10 00 00  1c e2 00 40 00 4a 00 40  |...?.......@.J.@|
00000040
```

From this we can see this is directly a ESP image file just as the esptool told us (no compression/encryption). We know this from the leading byte of 0xE9.
Following the table above we can see the following information

- Segement count: 1
- SPI Flash Interface: Digital Input/Output
- Flash size: 4 MB
- Processor Clock Rate: 40MHz
- Entry Point: 9c f2 10 40 (big endian)-> 40 10 f2 9c (little endian)

- Segment(1) memory offset: 00 f0 10 40 (big endian) -> 40 10 f0 00 (little endian) 
- Segment(1) segment size: 68 05 00 00 (big endian) -> 00 00 05 68 (little endian)
- Segment(1) data: ???????
---

## 5. Solution

## 6. Resources

1. [Reversing ESP8266 Firmware](https://boredpentester.com/reversing-esp8266-firmware-part-1/)
2. [Insomni’Hack 2017 write-up](https://phil242.wordpress.com/2017/02/07/inso2017-iof-400/)
3.