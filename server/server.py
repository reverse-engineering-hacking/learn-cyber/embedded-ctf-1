#!/usr/bin/env python3

#imports
from flask import Flask

#globals
app = Flask(__name__)

flag_key = '5up3r_53cr37_p455w0rd'

@app.route('/request/<key>')
def request(key=None):
    return xor_strings("No admin privileges!", key)

@app.route('/admin')
def retrieve_flag():
    return xor_strings("FLAG 2 OF 2 SECURED!!! HELL YEAH BROTHER!", flag_key)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def catch_all(path):
    return "Invalid path. Try again ;)"

def xor_strings_same_length(s, t):
    """xor two strings together"""
    if isinstance(s, str):
        # Text strings contain single characters
        return "".join(chr(ord(a) ^ ord(b)) for a, b in zip(s, t))
    else:
        # Python 3 bytes objects contain integer values in the range 0-255
        return bytes([a ^ b for a, b in zip(s, t)])

def repeat_to_length(string_to_expand, length):
    return (string_to_expand * (int(length/len(string_to_expand))+1))[:length]

def xor_strings(message, key):
    if len(message) == len(key):
        return xor_strings_same_length(message.encode('utf-8'), key.encode('utf-8'))
    elif len(message) > len(key):
        # The message is longer than the key
        return xor_strings_same_length(message.encode('utf-8'), repeat_to_length(key, len(message)).encode('utf-8'))
    elif len(message) < len(key):
        # The key is longer than the message
        return xor_strings_same_length(message.encode('utf-8'), key[:len(message)].encode('utf-8'))
        
if __name__ == '__main__':
    app.run(host="0.0.0.0")
