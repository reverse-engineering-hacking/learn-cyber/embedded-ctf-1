#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

char serverName[] = "http://szadeklord.dynu.net";
char requestAPI[] = "request";
char adminAPI[] = "admin";
char key[] = "DEADBEEF";
char SSID[] = "secret_net";
char WifiPass[] = "climbingisfun";
unsigned long IdleTime = 0;
char* UserCommands[] = { "fetch", "sysinfo", "login"};

/*
        Get data from server command - fetch
        Get local flag command - sysinfo
        Get server flag - login
*/

void connectToNetwork()
{
  WiFi.begin(SSID, WifiPass);

  Serial.print(String("Network:Attempting to connect to SSID: ") + String(SSID));
  
  unsigned long timeFinal = millis() + 15000;

  while (WiFi.status() != WL_CONNECTED && millis() < timeFinal)
  {
    delay(500);
    Serial.print(".");
  }
    
  Serial.println();

  if(WiFi.status() == WL_CONNECTED) 
  {

    Serial.print("Network:Connected, IP address: ");
    Serial.println("Network:" + WiFi.localIP());
  }
}

void setup()
{
  Serial.begin(115200);
  Serial.setTimeout(5000);
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
}

String xorDecrypt(const char* plainText, const char* key)
{
  char buff[80] = {};
  int textLen = strlen(plainText);
  int keyLen = strlen(key);
  int keyIndex = 0;

  for(int i = 0; i < textLen; ++i)
  {
    buff[i] = plainText[i] ^ key[keyIndex];

     if(keyIndex == keyLen - 1)
     {
       keyIndex = 0;
     }
     else
     {
       ++keyIndex;
     }
  }

  return String(buff);
}

void webserverRequest(char* aServerName, char* aAPIName, char* aAPIArg)
{
  if (WiFi.status() == WL_CONNECTED) 
  { 
    //Check WiFi connection status
    HTTPClient http;  //Declare an object of class HTTPClient

    String request = String(aServerName);
    request.concat('/'); 
    request.concat(aAPIName);
    
    if(aAPIArg != NULL)
    {
      request.concat('/');
      request.concat(aAPIArg);
    }

    http.begin(request);  //Specify request destination
    int httpCode = http.GET();                                                                  //Send the request
    if (httpCode > 0) { //Check the returning code
 
      String payload = http.getString();   //Get the request response payload
      
      if(aAPIArg != NULL)
      {
        Serial.println("Network:" + xorDecrypt(payload.c_str(), aAPIArg));                     //Print the response payload
      }
      else
      {
        Serial.println("Network:" + payload);                     //Print the response payload 
      }
    }
 
    http.end();   //Close connection
  }
  else
  {
    connectToNetwork();
  }
}

void FetchFnc() 
{
  String request = String(serverName);
  request.concat('/'); 
  request.concat(requestAPI); 
  Serial.println("Network:Requesting access to " + request);
  webserverRequest(serverName, requestAPI, key);
}

void SysinfoFnc() 
{ 
  Serial.println("FLAG 1 OF 2 SECURED!");

  __asm__ ("" : : "" ("5up3r_53cr37_p455w0rd"));
}

void LoginFnc() 
{
  String request = String(serverName);
  request.concat('/'); 
  request.concat(adminAPI); 
  Serial.println("Network:Requesting API data from " + request); 
  webserverRequest(serverName, adminAPI, NULL);
}

void (* UserCommandFunctions[3])() { FetchFnc, SysinfoFnc, LoginFnc};

void HandleInput(String command)
{
  int size = sizeof(UserCommands) / sizeof(char*);
  int matchIndex = 0;
  bool match = false;

  for(int i = 0; i < size; ++i)
  {
    if(strcmp(command.c_str(), UserCommands[i]) == 0)
    {
      matchIndex = i;
      match = true;
    }
  }

  if(match)
  {
    UserCommandFunctions[matchIndex]();
  }
  else
  {
    Serial.println("Input:Unknown command. Try again!");
    Serial.println("Input:Known commands: " + String(UserCommands[0]));
  }
}

void loop() 
{
  unsigned long preTime = millis();

  // Read the user input to determine if they entered a relevant command
  String serialInput = Serial.readString();

  unsigned long postTime = millis();

  // Determine if the user was inputting text or not
  // If not this would be counted as idle time.. so increment the idle time accordingly
  if(serialInput == "")
  {
    IdleTime = IdleTime + (postTime - preTime);
    Serial.println("Input:No commands given.");
  }
  else
  {
    serialInput.trim();
    HandleInput(serialInput);
  }
  
  // Only run the network portion of things every 15 seconds
  if(IdleTime > 15000)
  {
    IdleTime = 0;
   
    webserverRequest(serverName, requestAPI, key);
    
    
  }
}